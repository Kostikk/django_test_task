import json

import requests
from django.conf import settings
from django.contrib.auth.models import User
from rest_framework import serializers

from .models import Profile


class UserSerializer(serializers.ModelSerializer):
    """
    serialize user data and validate email using hunter.io api

    """
    password = serializers.CharField(max_length=65, min_length=8, write_only=True)
    username = serializers.CharField(max_length=60, min_length=2)
    email = serializers.EmailField(max_length=255, min_length=4)
    first_name = serializers.CharField(max_length=255, min_length=2, required=False)
    last_name = serializers.CharField(max_length=255, min_length=2, required=False)

    class Meta:
        model = User
        fields = ['id', 'username', 'first_name', 'last_name', 'email', 'password']

    def validate(self, attrs):
        email = attrs.get('email', '')
        username = attrs.get('username', '')
        api_key = settings.HUNTER_API_KEY

        url = 'https://api.hunter.io/v2/email-verifier?email=' + email + '&api_key=' + api_key
        response = requests.get(url)
        json_data = json.loads(response.text)

        if json_data['data']['status'] in ['invalid', 'disposable', 'unknown']:
            raise serializers.ValidationError(
                {'email': 'Your Email is incorrect'})

        if User.objects.filter(email=email).exists():
            raise serializers.ValidationError(
                {'email': 'Email is already in use'})

        elif User.objects.filter(username=username).exists():
            raise serializers.ValidationError(
                {'username': 'username is already in use'})
        return super().validate(attrs)

    def create(self, validated_data):
        return User.objects.create_user(**validated_data)


class ProfileSerializers(serializers.ModelSerializer):
    class Meta:
        model = Profile

        fields = ['user', 'first_login', 'ip_address', 'github', 'facebook', 'bio']


class LoginSerializer(serializers.ModelSerializer):
    password = serializers.CharField(
        max_length=65, min_length=8, write_only=True)
    username = serializers.CharField(max_length=255, min_length=2)

    class Meta:
        model = User
        fields = ['username', 'password']
