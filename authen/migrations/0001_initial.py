# Generated by Django 3.1.3 on 2020-11-19 21:39

import django.db.models.deletion
from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Profile',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('first_login', models.DateTimeField(auto_now_add=True)),
                ('ip_address', models.GenericIPAddressField(default='0.0.0.0', verbose_name='ip address')),
                ('github', models.TextField(blank=True, max_length=500, null=True)),
                ('facebook', models.TextField(blank=True, max_length=500, null=True)),
                ('bio', models.TextField(blank=True, null=True)),
                ('user', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
