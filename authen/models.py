from django.contrib.auth.models import User
from django.db import models


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    first_login = models.DateTimeField(auto_now_add=True)
    ip_address = models.GenericIPAddressField(default='0.0.0.0', verbose_name="ip address")
    github = models.TextField(max_length=500, blank=True, null=True)
    facebook = models.TextField(max_length=500, blank=True, null=True)
    bio = models.TextField(blank=True, null=True)
    updated = models.DateTimeField(auto_now=True, null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True, null=True, blank=True)
