import clearbit
import jwt
from django.conf import settings
from django.contrib import auth
from rest_framework import status
from rest_framework.generics import GenericAPIView
from rest_framework.response import Response

from .models import Profile
from .serializers import UserSerializer, LoginSerializer


class RegisterView(GenericAPIView):
    serializer_class = UserSerializer

    def post(self, request):
        """Create usre with validating email and request to clearbit

        uncoment response2 variable to use clearbit api
        """

        serializer = UserSerializer(data=request.data)

        response2 = clearbit.Enrichment.find(email=request.data['email'], stream=True)
        # response2 = clearbit.Enrichment.find(email='alex@clearbit.com', stream=True)

        if response2 is not None:
            if 'first_name' not in request.data:
                request.data['first_name'] = response2['person']['name']['givenName']
            if 'last_name' not in request.data:
                request.data['last_name'] = response2['person']['name']['familyName']

        if serializer.is_valid():
            serializer.save()
            if response2 is not None:
                profile_data = Profile(user_id=serializer.data['id'],
                                       facebook='facebook.com/' + response2['person']['facebook']['handle'],
                                       github='github.com/' + response2['person']['github']['handle'],
                                       bio=response2['person']['bio']
                                       )
                profile_data.save()
            else:
                profile_data = Profile(user_id=serializer.data['id'],
                                       facebook=None,
                                       github=None,
                                       bio=None
                                       )
                profile_data.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class LoginView(GenericAPIView):
    serializer_class = LoginSerializer

    def post(self, request):
        data = request.data
        username = data.get('username')
        password = data.get('password')

        user = auth.authenticate(username=username, password=password)

        if user:
            auth_token = jwt.encode(
                {'username': user.username}, settings.JWT_SECRET_KEY)
            serializer = UserSerializer(user)

            data = {'user': serializer.data, 'token': auth_token}
            x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
            if x_forwarded_for:
                ip = x_forwarded_for.split(',')[0]
            else:
                ip = request.META.get('REMOTE_ADDR')

            profile_data = Profile.objects.get(user_id=serializer.data['id'])
            profile_data.ip_address = ip
            profile_data.save()
            return Response(data, status=status.HTTP_200_OK)

        return Response({'detail': 'Invalid credentials'}, status=status.HTTP_401_UNAUTHORIZED)
