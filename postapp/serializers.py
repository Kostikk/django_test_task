from rest_framework.serializers import ModelSerializer

from .models import Post_table, Like


class Post_tableSerializer(ModelSerializer):
    class Meta:
        model = Post_table

        fields = ['id', 'title', 'body', 'author_id', 'created', 'updated']


class LikeSerializer(ModelSerializer):
    class Meta:
        model = Like

        fields = ['id', 'created', 'updated', 'post_id', 'user_id']


class UnlikeSerializer(ModelSerializer):
    class Meta:
        model = Like

        fields = ['id', 'created', 'updated', 'post_id', 'user_id']

