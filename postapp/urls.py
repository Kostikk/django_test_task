from django.urls import path

from .views import NewPost, LikePost, EventDelete

urlpatterns = [
    path('', NewPost.as_view()),
    path('like', LikePost.as_view()),
    path('deletelikes/<pk>',EventDelete.as_view(), name='delete_event')


]
