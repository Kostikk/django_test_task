from django.db import models

from authen.models import User


class Post_table(models.Model):
    author = models.ForeignKey(User, on_delete=models.CASCADE, related_name='posts', unique=False)
    title = models.CharField(max_length=200)
    body = models.TextField()
    likes = models.ManyToManyField(User, related_name='likes', blank=True)
    updated = models.DateTimeField(auto_now=True, null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True, null=True, blank=True)

    def __str__(self):
        return str(self.body[:20])

    def num_likes(self):
        return self.likes.all().count()


LIKE_CHOICES = (
    ('Like', 'Like'),
    ('Unlike', 'Unlike'),
)


class Like(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    post = models.ForeignKey(Post_table, on_delete=models.CASCADE)
    value = models.CharField(choices=LIKE_CHOICES, max_length=8)
    updated = models.DateTimeField(auto_now=True)
    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f"{self.user}-{self.post}-{self.value}"
