from rest_framework import permissions, status
from rest_framework.decorators import api_view
from rest_framework.generics import ListCreateAPIView, DestroyAPIView
from rest_framework.response import Response
from rest_framework.views import APIView

from .models import Like
from .serializers import Post_tableSerializer, LikeSerializer, UnlikeSerializer
from django.http import HttpResponse, Http404


class NewPost(ListCreateAPIView):
    """create post"""
    serializer_class = Post_tableSerializer
    permission_classes = (permissions.IsAuthenticated,)

    def perform_create(self, serializer):
        # create post
        serializer.save(author=self.request.user)
    #
    # def get_queryset(self):
    #     return Post_table.objects.filter(author=self.request.user)


# class Post_tableDetailView(RetrieveUpdateDestroyAPIView):
#     serializer_class = LikeSerializer
#     permission_classes = (permissions.IsAuthenticated,)
#     lookup_field = "id"
#
#     def get_queryset(self):
#         return Post_table.objects.filter(author=self.request.user)


class LikePost(ListCreateAPIView):
    """Like post"""

    serializer_class = LikeSerializer
    permission_classes = (permissions.IsAuthenticated,)

    def perform_create(self, serializer):
        like = {'user_id': self.request.user.id, 'post_id': int(self.request.data['post_id'])}
        like_data = Like.objects.values('user_id', 'post_id')

        serializer.save(user_id=str(self.request.user.id), post_id=self.request.data['post_id'])
        print('Liked')


# @api_view(["DELETE"])
# def product_delete_rest_endpoint(request):
#     serializer_class = LikeSerializer
#     permission_classes = (permissions.IsAuthenticated,)
#     Like.objects.filter(post_id=request.data['post_id']).delete()
#     exit('1488 nigga')
#     return Response()


class EventDelete(APIView):

    serializer_class = LikeSerializer
    permission_classes = (permissions.IsAuthenticated,)

    def get_object(self, pk):
        try:
            return Like.objects.filter(post_id=pk)
        except Like.DoesNotExist:
            raise Http404

    def delete(self, request, pk, format=None):
        like = self.get_object(pk)
        like.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

    #
#
# class UnlikePost(DestroyAPIView):
#     """unlike post"""
#
#     serializer_class = UnlikeSerializer
#     permission_classes = (permissions.IsAuthenticated,)
#
#     def get_queryset(self):
#         return
#
#     def perform_delete(self, serializer):
#
#         Like.objects.filter(post_id=self.request.data['post_id']).delete()
#         # Like.objects.filter(user_id=str(self.request.user.id), post_id=self.request.data['post_id']).delete()
#
#
# # SomeModel.objects.filter(id=id).delete()
