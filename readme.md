## Test task Python, django - install and start

#1
`pip3 install -r requirements.txt`

#2
`python3 manage.py migrate`

#3
run server

#4
`python3 projTest.py` to run autobot


## API doc

api list
|number |method |url                   |format                                                                          |
|:-----:|:-----:|:--------------------:|:------------------------------------------------------------------------------:|
|1      |POST   |/api/auth/registration|{ "username": "annabelle","email": "mailmail@gmail.com","password": "123456789"}|
|2      |POST   |/api/auth/login       |{ "username": "annabelle","password": "159159159"}                              |
|3      |POST   |/api/posts/           |{  "title": "posttitle","body": "postbody"} (with bearer token in headers)      |
|4      |POST   |/api/posts/liked      |{ "post_id": "4"} (with bearer token in headers)                                |
|5      |DELETE |/api/deletelikes/{id} | (with bearer token in headers)                                                 |




authen/serializers.py uses hunter.io api 
authen/views.py uses clearbit api  
uncoment 25 string in authen/views.py to ckeck it or use alex@clearbit.com for check it

Use postman for test
User auth use Bearer token in headers of request 

example:

Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VybmFtZSI6IjQzMjQzMmJsYSJ9.DzAA5zsx5cwAoZs0GTKfZpjbAaUHXcCYOnAo0VjWWOI

