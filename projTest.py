import json
import random
import string
import requests
import configparser
import os
import django

# os.environ.setdefault("DJANGO_SETTINGS_MODULE", "testproject.settings")
# django.setup()
#
# from postapp.models import Post_table
#
# # load data  from configfile
from mintcommon.apt_changelog import __init__


# post_ids = Post_table.objects.filter().values_list('id', flat=True)
# print(list(post_ids))


class TestAPI:

    def __init__(self):
        self.configParser = configparser.RawConfigParser()
        self.configFilePath = 'configfile'
        self.configParser.read(self.configFilePath)
        self.details_dict = dict(self.configParser.items('BOTCONFIG'))

        print('new users = ' + self.details_dict['number_of_users'])
        print('post per user = ' + self.details_dict['max_post_per_user'])
        print('Like per user = ' + self.details_dict['max_like_per_user'])

        self.number_of_users = int(self.details_dict['number_of_users'])
        self.max_post_per_user = int(self.details_dict['max_post_per_user'])
        self.max_like_per_user = int(self.details_dict['max_like_per_user'])

        self.generate_userdata = []

    def register_action(self):
        for i in range(self.number_of_users):

            try:
                # generate username, email, password

                letters_and_digits = string.ascii_letters + string.digits

                result_username = ''.join((random.choice(letters_and_digits) for a in range(15)))
                result_email = ''.join((random.choice(letters_and_digits) for b in range(15)))
                result_password = ''.join((random.choice(letters_and_digits) for c in range(10)))
                userdata = {
                    "username": result_username,
                    "email": result_email,
                    "password": result_password
                }
                self.generate_userdata.append(userdata)

                print('new iteration')
                print('---------------------------------------')
                print('generated username: ' + result_username)
                print('generated email: ' + result_email + "@gmail.com")
                print('generated password: ' + result_password)
                # request for user signup
                request1 = requests.post('http://127.0.0.1:8000/api/auth/registration', json={
                    "username": result_username,
                    "email": result_email + "@gmail.com",
                    "password": result_password
                })

                print(request1.text)
                print(self.generate_userdata)
                print('registration is success')
            except Exception as e:  # This is the correct syntax
                # exit('server not avaiable')
                raise SystemExit(e)


testrequest = TestAPI()
testrequest.register_action()

    # def login_action(self):
    #     for i in range(self.number_of_users):
    #         try:
    #             # request for user login
    #             request2 = requests.post('http://127.0.0.1:8000/api/auth/login', json={
    #                 "username": result_username,
    #                 "password": result_password
    #             })
    #             # request2 = requests.post('http://127.0.0.1:8000/api/auth/login', json={
    #             #     "username": "432432bla",
    #             #     "password": "123456789"
    #             # })
    #             token_data = json.loads(request2.text)
    #             token = 'Bearer ' + token_data['token']
    #             headers = {'Authorization': token}
    #             print('usertoken = ' + token_data['token'])
    #             print('auth is success')
    #             print('')
    #             for j in range(max_post_per_user):
    #                 # generete username, email, password
    #
    #                 result_title = ''.join((random.choice(letters_and_digits) for d in range(10)))
    #                 result_body = ''.join((random.choice(letters_and_digits) for f in range(80)))
    #                 # request for user post creation
    #                 request3 = requests.post('http://127.0.0.1:8000/api/posts/',
    #                                          json={
    #                                              "title": result_title,
    #                                              "body": result_body
    #                                          },
    #                                          headers=headers
    #                                          )
    #                 time_data = json.loads(request3.text)
    #
    #                 print('post created, date = ' + time_data['created'])
    #                 print('')
    #         except Exception as e:  # This is the correct syntax
    #             # exit('server not avaiable')
    #             raise SystemExit(e)




            # for m in range(max_like_per_user):
            #     # request for user post like
            #     random_post_id = str(random.choice(post_ids))
            #     print('')
            #     print('Like post id = ' + random_post_id)
            #     request4 = requests.post('http://127.0.0.1:8000/api/posts/liked',
            #                              json={
            #                                  "post_id": random_post_id
            #                              },
            #                              headers=headers
            #                              )
            #     like_date = json.loads(request4.text)
            #     print('post liked, date = ' + like_date['created'])
            #     print('like id = ' + str(like_date['id']))
            #     print('post id = ' + like_date['post_id'])
            #     print('user owner post id = ' + like_date['user_id'])
            #     print('')
            # print('---------------------------------------')

#
#
#
# for i in range(number_of_users):
#
#     try:
#         #generete username, email, password
#         letters_and_digits = string.ascii_letters + string.digits
#         result_username = ''.join((random.choice(letters_and_digits) for a in range(15)))
#         result_email = ''.join((random.choice(letters_and_digits) for b in range(15)))
#         result_password = ''.join((random.choice(letters_and_digits) for c in range(10)))
#         print('new iteration')
#         print('---------------------------------------')
#         print('generated username: ' + result_username)
#         print('generated email: ' + result_email + "@gmail.com")
#         print('generated password: ' + result_password)
#         # request for user signup
#         request1 = requests.post('http://127.0.0.1:8000/api/auth/registration', json={
#             "username": result_username,
#             "email": result_email + "@gmail.com",
#             "password": result_password
#         })
#
#         print(request1.text)
#         print('registration is success')
#         # request for user login
#         request2 = requests.post('http://127.0.0.1:8000/api/auth/login', json={
#             "username": result_username,
#             "password": result_password
#         })
#         # request2 = requests.post('http://127.0.0.1:8000/api/auth/login', json={
#         #     "username": "432432bla",
#         #     "password": "123456789"
#         # })
#         token_data = json.loads(request2.text)
#         token = 'Bearer ' + token_data['token']
#         headers = {'Authorization': token}
#         print('usertoken = ' + token_data['token'])
#         print('auth is success')
#         print('')
#         for j in range(max_post_per_user):
#             # generete username, email, password
#
#             result_title = ''.join((random.choice(letters_and_digits) for d in range(10)))
#             result_body = ''.join((random.choice(letters_and_digits) for f in range(80)))
#             # request for user post creation
#             request3 = requests.post('http://127.0.0.1:8000/api/posts/',
#                                      json={
#                                          "title": result_title,
#                                          "body": result_body
#                                      },
#                                      headers=headers
#                                      )
#             time_data = json.loads(request3.text)
#
#             print('post created, date = ' + time_data['created'])
#             print('')
#
#         for m in range(max_like_per_user):
#             # request for user post like
#             random_post_id = str(random.choice(post_ids))
#             print('')
#             print('Like post id = ' + random_post_id)
#             request4 = requests.post('http://127.0.0.1:8000/api/posts/liked',
#                                      json={
#                                          "post_id": random_post_id
#                                      },
#                                      headers=headers
#                                      )
#             like_date = json.loads(request4.text)
#             print('post liked, date = ' + like_date['created'])
#             print('like id = ' + str(like_date['id']))
#             print('post id = ' + like_date['post_id'])
#             print('user owner post id = ' + like_date['user_id'])
#             print('')
#         print('---------------------------------------')
#     except Exception as e:  # This is the correct syntax
#         # exit('server not avaiable')
#         raise SystemExit(e)
